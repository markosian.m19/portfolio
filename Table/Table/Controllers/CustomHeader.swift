//
//  CustomHeader.swift
//  Table
//
//  Created by Марат Маркосян on 28.11.2021.
//

import UIKit

class CustomHeader: UITableViewHeaderFooterView {

    let title = UILabel()
    let image = UIImageView()
    let button = UIButton()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        configureContents()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
    private func configureContents() {
        image.translatesAutoresizingMaskIntoConstraints = false
        title.translatesAutoresizingMaskIntoConstraints = false
        button.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(image)
        contentView.addSubview(title)
        contentView.addSubview(button)
        
        NSLayoutConstraint.activate([
            image.leadingAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leadingAnchor),
            image.widthAnchor.constraint(equalToConstant: Constants.imageHeightWidth),
            image.heightAnchor.constraint(equalToConstant: Constants.imageHeightWidth),
            image.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        
            title.heightAnchor.constraint(equalToConstant: Constants.fifteen),
            title.leadingAnchor.constraint(equalTo: image.trailingAnchor,
                                           constant: Constants.eight),
            title.trailingAnchor.constraint(equalTo:
                   contentView.layoutMarginsGuide.trailingAnchor),
            title.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            
            button.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            button.leadingAnchor.constraint(equalTo: contentView.leadingAnchor)
        ])
        
    }
        
}
