//
//  DetailViewController.swift
//  Table
//
//  Created by Марат Маркосян on 28.11.2021.
//

import UIKit

class DetailViewController: UIViewController {
    
    lazy var nameLabel = UILabel()
    lazy var animalImage = UIImageView()
        
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpSubviews()
        setUpAutoLayout()
    }
    
    private func setUpSubviews() {
        view.backgroundColor = .white
        view.addSubview(nameLabel)
        view.addSubview(animalImage)
        
        animalImage.contentMode = .scaleAspectFit
        nameLabel.font = nameLabel.font.withSize(Constants.thirty)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        animalImage.translatesAutoresizingMaskIntoConstraints = false
        
    }
    
    private func setUpAutoLayout() {
        NSLayoutConstraint.activate([
            animalImage.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            animalImage.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            animalImage.heightAnchor.constraint(equalToConstant: Constants.hundred),
            animalImage.widthAnchor.constraint(equalToConstant: Constants.hundred),
            
            nameLabel.topAnchor.constraint(equalTo: animalImage.bottomAnchor),
            nameLabel.centerXAnchor.constraint(equalTo: animalImage.centerXAnchor)
        ])
    }

}
