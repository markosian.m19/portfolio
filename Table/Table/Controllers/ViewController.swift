//
//  ViewController.swift
//  Table
//
//  Created by Марат Маркосян on 28.11.2021.
//

import UIKit

class ViewController: UIViewController {
    
    lazy var tableView = UITableView()
    
    var cellsContent = [
        CellContent(image: UIImage(systemName: "hare")!, name: Constants.hareName),
        CellContent(image: UIImage(systemName: "ant")!, name: Constants.antName),
        CellContent(image: UIImage(systemName: "tortoise")!, name: Constants.tortoiseName)
    ]
    var refresher = UIRefreshControl()
    var sectionHidden = false

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpSubviews()
        setUpAutoLayout()
    }

    private func setUpSubviews() {
        view.backgroundColor = .white
        
        tableView.dataSource = self
        tableView.delegate = self
        
        view.addSubview(tableView)
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(UINib(nibName: Constants.nibForCell, bundle: nil), forCellReuseIdentifier: Constants.reusableIdentifierForCell)
        tableView.register(CustomHeader.self, forHeaderFooterViewReuseIdentifier: Constants.reusableIdentifierForHeader)
        tableView.addSubview(refresher)
        
        refresher.attributedTitle = NSAttributedString(string: Constants.refresherCommand)
        refresher.addTarget(self, action: #selector(refresh), for: .valueChanged)

        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(cellAdded))
    }

    private func setUpAutoLayout() {
        NSLayoutConstraint.activate([
            tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    @objc private func refresh() {
        tableView.reloadData()
        refresher.endRefreshing()
    }
    
    @objc private func cellAdded() {
        let newContent = CellContent(image: UIImage(systemName: "flame")!, name: Constants.newString)
        cellsContent.append(newContent)
        tableView.reloadData()
    }

}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if sectionHidden {
            return 0
        }
        return cellsContent.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constants.reusableIdentifierForCell, for: indexPath) as? CustomTableViewCell {
            cell.label.text = cellsContent[indexPath.row].name
            cell.imageForCell.image = cellsContent[indexPath.row].image
            return cell
        }
        return UITableViewCell()
    }


    func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        return Constants.footerName
    }

}

extension ViewController: UITableViewDelegate {
    

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            cellsContent.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: Constants.reusableIdentifierForHeader) as? CustomHeader {
            header.title.text = Constants.headerName
            header.image.image = UIImage(systemName: "bolt")
            header.button.addTarget(self, action: #selector(hide), for: .touchUpInside)
            return header
        }
        
        return UIView()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return Constants.thirty
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let presentedView = DetailViewController()
        presentedView.nameLabel.text = cellsContent[indexPath.row].name
        presentedView.animalImage.image = cellsContent[indexPath.row].image
        
        present(presentedView, animated: true, completion: nil)
    }
    
    @objc private func hide() {
        if !sectionHidden {
            sectionHidden = true
        } else {
            sectionHidden = false
        }
        tableView.reloadData()
    }
}



