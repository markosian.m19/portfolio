//
//  CellContent.swift
//  Table
//
//  Created by Марат Маркосян on 28.11.2021.
//

import UIKit

struct CellContent: Hashable {
    let image: UIImage
    let name: String
}
