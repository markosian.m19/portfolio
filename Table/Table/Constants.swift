//
//  Constants.swift
//  Table
//
//  Created by Марат Маркосян on 30.11.2021.
//

import UIKit

struct Constants {
    
//MARK:- Strings
    static let nibForCell = "CustomTableViewCell"
    static let reusableIdentifierForCell = "ReusableCell"
    static let reusableIdentifierForHeader = "sectionHeader"
    static let refresherCommand = "pull to update"
    static let footerName = "List of animals"
    static let headerName = "Animals"
    static let allertTitle = "New cell added!"
    static let allertMessage = "To display new cells refresh it."
    static let okString = "OK"
    static let addTitle = "Add"
    static let newString = "NEW"
    static let hareName = "Hare"
    static let antName = "Ant"
    static let tortoiseName = "Tortoise"
    
//MARK:- Numbers
    static let imageHeightWidth: CGFloat = 25
    static let fifteen: CGFloat = 15
    static let eight: CGFloat = 8
    static let thirty: CGFloat = 30
    static let hundred: CGFloat = 100
}
