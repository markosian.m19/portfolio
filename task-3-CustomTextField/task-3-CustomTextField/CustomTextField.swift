//
//  CustomTextField.swift
//  task-3-CustomTextField
//
//  Created by Марат Маркосян on 01.10.2021.
//

import UIKit

class CustomTextField: UITextField {
    
    private enum Constants {
        static let placeholderLabelText = "Card number"
        static let requiredNumberOfChars = 16
        
        case bigFont
        case defaultFont
        case placeFromBottomAnchor
        case borderWidth
        case placeFromTopAnchor
        
        var selectionProcessing: CGFloat {
            switch self {
            case .bigFont:
                return 20
            case .defaultFont:
                return 13
            case .placeFromBottomAnchor:
                return 10
            case .borderWidth:
                return 2
            case .placeFromTopAnchor:
                return 15
            }
        }

    }
    
    var textFieldIsFocusedCallback: (() -> Void)?
    var textFieldLostFocusCallback: (() -> Void)?
    var returnKeyIsPressCallback: (() -> Void)?
    var textChangedCallback: (() -> Void)?
    var requiredNumberOfChars: Int?
    
    lazy var placeholderLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.text = Constants.placeholderLabelText
        label.textColor = .lightGray
        label.font = label.font.withSize(Constants.bigFont.selectionProcessing)
        return label
    }()
    
    lazy var textCounter: UILabel = {
        let text = UILabel()
        text.textColor = .lightGray
        text.font = text.font.withSize(Constants.defaultFont.selectionProcessing)
        text.isHidden = true
        return text
    }()
    
    lazy var border: CALayer = {
        let border = CALayer()
        let width: CGFloat = Constants.borderWidth.selectionProcessing
        
        border.borderColor = UIColor.green.cgColor
        border.frame = CGRect(x: 0, y: frame.size.height - width, width:  UIScreen.main.bounds.width, height: frame.size.height)
        border.borderWidth = width
        border.isHidden = true
        return border
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUpSubviews()
        setUpAutoLayout()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setUpSubviews()
        setUpAutoLayout()
    }
    
    private func setUpSubviews() {
        addSubview(placeholderLabel)
        addSubview(textCounter)
        layer.addSublayer(border)
        addTarget(self, action: #selector(textViewDidChange), for: .editingChanged)
        delegate = self
        layer.masksToBounds = true
    }
    
    private func setUpAutoLayout() {
        placeholderLabel.translatesAutoresizingMaskIntoConstraints = false
        textCounter.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            placeholderLabel.leftAnchor.constraint(equalTo: leftAnchor),
            placeholderLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -Constants.placeFromBottomAnchor.selectionProcessing),
            placeholderLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: -Constants.placeFromTopAnchor.selectionProcessing),
            textCounter.rightAnchor.constraint(equalTo: rightAnchor),
            textCounter.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    @objc private func textViewDidChange() {
        let textNum = text?.count ?? 0
        let requiredNumberOfChars = requiredNumberOfChars ?? Constants.requiredNumberOfChars
        textColor = textNum == requiredNumberOfChars ? .blue : .red
        textChangedCallback?()
        textCounter.text = "Chars: \(textNum)/\(requiredNumberOfChars)"
    }
}

extension CustomTextField: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textFieldIsFocusedCallback?()
        
        textCounter.isHidden = false
        border.isHidden = false
        placeholderLabel.font = placeholderLabel.font.withSize(Constants.defaultFont.selectionProcessing)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        border.isHidden = true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        resignFirstResponder()
        returnKeyIsPressCallback?()
        if text?.count == 0 {
            placeholderLabel.font = placeholderLabel.font.withSize(Constants.bigFont.selectionProcessing)
        }
        return true
    }
}

