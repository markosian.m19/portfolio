//
//  ViewController.swift
//  task-3-CustomTextField
//
//  Created by Марат Маркосян on 01.10.2021.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var customTextField: CustomTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpSubviews()
        setUpAutoLayout()
    }

    private func setUpSubviews() {
        customTextField.keyboardType = .default
        customTextField.returnKeyType = .go
    }
    
    private func setUpAutoLayout() {
        view.backgroundColor = .systemBackground
    }
}

