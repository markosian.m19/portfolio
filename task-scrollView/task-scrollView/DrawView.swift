//
//  DrawView.swift
//  task-scrollView
//
//  Created by Марат Маркосян on 20.10.2021.
//

import UIKit

struct Stroke {
    let startPoint: CGPoint
    let endPoint:  CGPoint
    let color: CGColor
}

class DrawView: UIView {

    private var firstTouchPoint: CGPoint?
    private var lastTouchPoint: CGPoint?
    private var moved = false
    private var color = UIColor.green.cgColor
    private var strokes = [Stroke]()
    let longTap = UILongPressGestureRecognizer(target: self, action: #selector(clearView))

    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setUpTap()
    }
    
    private func setUpTap() {
        addGestureRecognizer(longTap)
        longTap.minimumPressDuration = 2
        longTap.numberOfTouchesRequired = 1
        longTap.allowableMovement = 20
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        firstTouchPoint = touches.first?.location(in: self)
        lastTouchPoint = firstTouchPoint
        setNeedsDisplay()
        moved = false

    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)
        moved = true
        setNeedsDisplay()
        firstTouchPoint = touches.first?.location(in: self)
        guard let last = lastTouchPoint, let first = firstTouchPoint else { return }
        let sstroke = Stroke(startPoint: last, endPoint: first, color: color)
        strokes.append(sstroke)
        lastTouchPoint = firstTouchPoint
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        lastTouchPoint = touches.first?.location(in: self)
        guard let last = lastTouchPoint, let first = firstTouchPoint else { return }
        let sstroke = Stroke(startPoint: last, endPoint: first, color: color)
        strokes.append(sstroke)
        UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        setNeedsDisplay()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)

        drawLines()
    }
        
    private func drawLines() {
        guard let context = UIGraphicsGetCurrentContext() else { return }
        context.setLineWidth(15)
        context.setLineCap(.round)
        for stroke in strokes {
            context.beginPath()
            context.setStrokeColor(CGColor.init(red: 0, green: 1, blue: 0, alpha: 1))
            context.move(to: stroke.startPoint)
            context.addLine(to: stroke.endPoint)
            context.strokePath()
            moved = true
        }
    }
    
    @objc private func clearView(gesture: UILongPressGestureRecognizer) {
        if gesture.state == .began {
        let context = UIGraphicsGetCurrentContext()
        context?.clear(bounds)
        context?.setFillColor(backgroundColor?.cgColor ?? UIColor.clear.cgColor)
        context?.fill(bounds)
        strokes.removeAll()
        setNeedsDisplay()
    }
    }
    
}
