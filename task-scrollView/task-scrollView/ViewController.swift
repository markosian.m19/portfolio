//
//  ViewController.swift
//  task-scrollView
//
//  Created by Марат Маркосян on 20.10.2021.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var draw: DrawView!

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .systemBackground
    }

}

