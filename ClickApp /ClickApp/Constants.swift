//
//  Constants.swift
//  ClickApp
//
//  Created by Марат Маркосян on 01.02.2022.
//

import UIKit

struct Constants {
    
    //MARK: - Numbers
    static let oneHundred: CGFloat = 100
    static let ten: CGFloat = 10
    static let thirty: CGFloat = 30
    static let zero = 0.0
    static let millisecond = 0.01
    static let thousand = 1000.0
    static let seven = 7.0
    
    //MARK: - Strings
    static let gameRules = """
                        ➣To win you have to touch the aim 10 times faster than 7 seconds
                        ➣The aim appears in a random position
                        ➣The game starts after you click "Got it"
                        Good luck!
                        """
    static let rulesLabel = "Rules"
    static let gotIt = "Got it!"
    static let sevenString = "7.00"
    static let aim = "aim"
    static let mainBackground = "mainBackground"
    
}
