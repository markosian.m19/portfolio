//
//  ViewController.swift
//  Test_task_ANODA
//
//  Created by Марат Маркосян on 07.02.2022.
//

import UIKit
import CoreLocation

class ViewController: UIViewController {
    
    private lazy var backgroundImage = UIImageView(image: UIImage(named: "backgroundMain"))
    private lazy var locationButton = UIButton()
    private lazy var searchTextField = UITextField()
    private lazy var cityLabel = UILabel()
    private lazy var temperatureLabel = UILabel()
    private lazy var clcLabel = UILabel()
    private lazy var conditionImage = UIImageView(image: UIImage(systemName: "sun.max"))
    private let stack = UIStackView()
    
    var weatherManager = WeatherManager()
    let locationManager = CLLocationManager()

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpSubviews()
        setUpAutoLayout()
    }

    private func setUpSubviews() {
        view.backgroundColor = .systemBackground
        
        searchTextField.delegate = self
        weatherManager.delegate = self
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        
        view.addSubview(backgroundImage)
        view.addSubview(searchTextField)
        view.addSubview(locationButton)
        view.addSubview(stack)
        view.addSubview(conditionImage)
        view.addSubview(temperatureLabel)
        view.addSubview(clcLabel)
        view.addSubview(cityLabel)
        
        backgroundImage.contentMode = .scaleAspectFill
        backgroundImage.translatesAutoresizingMaskIntoConstraints = false
        searchTextField.translatesAutoresizingMaskIntoConstraints = false
        locationButton.translatesAutoresizingMaskIntoConstraints = false
        conditionImage.translatesAutoresizingMaskIntoConstraints = false
        temperatureLabel.translatesAutoresizingMaskIntoConstraints = false
        clcLabel.translatesAutoresizingMaskIntoConstraints = false
        cityLabel.translatesAutoresizingMaskIntoConstraints = false
        
        searchTextField.textAlignment = .right
        searchTextField.placeholder = "Search"
        searchTextField.autocapitalizationType = .words
        searchTextField.font = UIFont.systemFont(ofSize: 25)
        searchTextField.borderStyle = .roundedRect
        searchTextField.returnKeyType = .go
        searchTextField.alpha = 0.5
        
        stack.axis = .horizontal
        stack.alignment = .fill
        stack.distribution = .fill
        stack.spacing = 10
        stack.contentMode = .scaleToFill
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.addSubview(locationButton)
        stack.addSubview(searchTextField)
        
        temperatureLabel.font = UIFont.boldSystemFont(ofSize: 80)
        clcLabel.font = UIFont.boldSystemFont(ofSize: 80)
        cityLabel.font = UIFont.systemFont(ofSize: 30)
        cityLabel.text = "Kyiv"
        clcLabel.text = "ºC"
        temperatureLabel.text = "10"
        
        
        locationButton.setBackgroundImage(UIImage(systemName: "location.circle.fill"), for: .normal)
        locationButton.addTarget(self, action: #selector(locationPressed), for: .touchUpInside)
        locationButton.tintColor = .black
        conditionImage.tintColor = .black
    }
    
    private func setUpAutoLayout() {
        NSLayoutConstraint.activate([
            backgroundImage.topAnchor.constraint(equalTo: view.topAnchor),
            backgroundImage.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            backgroundImage.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            backgroundImage.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            stack.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 10),
            stack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            stack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10),
            stack.heightAnchor.constraint(equalToConstant: 40),
            stack.centerXAnchor.constraint(equalTo: view.centerXAnchor),

            locationButton.widthAnchor.constraint(equalToConstant: 40),
            locationButton.heightAnchor.constraint(equalToConstant: 40),
            locationButton.centerYAnchor.constraint(equalTo: stack.centerYAnchor),
            locationButton.leadingAnchor.constraint(equalTo: stack.leadingAnchor),
            
            searchTextField.trailingAnchor.constraint(equalTo: stack.trailingAnchor),
            searchTextField.leadingAnchor.constraint(equalTo: locationButton.trailingAnchor),
            searchTextField.centerYAnchor.constraint(equalTo: stack.centerYAnchor),
            
            conditionImage.widthAnchor.constraint(equalToConstant: 120),
            conditionImage.heightAnchor.constraint(equalToConstant: 120),
            conditionImage.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            conditionImage.topAnchor.constraint(equalTo: stack.bottomAnchor, constant: 10),
            
            clcLabel.topAnchor.constraint(equalTo: conditionImage.bottomAnchor),
            clcLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10),
            
            temperatureLabel.trailingAnchor.constraint(equalTo: clcLabel.leadingAnchor),
            temperatureLabel.topAnchor.constraint(equalTo: conditionImage.bottomAnchor),
            
            cityLabel.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -10),
            cityLabel.topAnchor.constraint(equalTo: clcLabel.bottomAnchor)

        ])
    }
    
    @objc private func locationPressed() {
        locationManager.requestLocation()
    }

}

extension ViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        searchTextField.endEditing(true)
        return true
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField.text != "" {
            return true
        } else {
            textField.placeholder = "Type something"
            return false
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let city = searchTextField.text {
            print(weatherManager.fetchWeather(city: city))
        }
        
        searchTextField.text = ""
    }
}

extension ViewController: WeatherManagerDelegate {
    func didUpdateWeather(_ weatherManager: WeatherManager, weather: WeatherModel) {
        DispatchQueue.main.async {
            self.temperatureLabel.text = weather.tempString
            self.conditionImage.image = UIImage(systemName: weather.conditionName)
            self.cityLabel.text = weather.cityName
        }
    }
    
    func didFailWithError(error: Error) {
        print(error)
    }
}

extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            locationManager.stopUpdatingLocation()
            let lat = location.coordinate.latitude
            let lon = location.coordinate.longitude
            weatherManager.fetchWeather(latitude: lat, longitude: lon)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error)
    }
}

